class Block:
  # Represents a Block, which holds a position and a colour
  def __init__(self, x, y, hue):
    self.x = x
    self.y = y
    self.hue = hue