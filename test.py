# Python Standard Libraries
import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import csv 
import ast
import logging

# Image Processing
import imutils
import cv2 as cv
from skimage.io import imread_collection

### Import ImageToEffects ###
from imagetoeffects import imageToEffects, convertToAudio
from effect import Effect

### Utility Functions ###
def areClose(a, b, l=10):
  return abs(a - b) <= l

def getExpectedFromFile(expectedDataFile):
  fields = [] 
  rows = [] 

  # Reading csv file 
  with open(expectedDataFile, 'r') as csvfile: 
    # Creating a csv reader object 
    csvreader = csv.reader(csvfile, delimiter='\t') 
    # Extracting field names in the first row 
    fields = next(csvreader) 
    # Extracting each data row one by one 
    for row in csvreader: 
      rows.append(ast.literal_eval(row[0]))

  return rows

### State Setup ###
firstFrame = None

redHue = 178
blueHue = 108
greenHue = 65
brownHue = 16

expectedEffects = [
  Effect("Reverb", "Room Size", "Wet", blueHue), 
  Effect("Delay", "Rate", "Wet", redHue), 
  Effect("Distortion", "Growl", "Wet", greenHue),
  Effect("Auto-Wah", "Rate", "Tone", brownHue)
]

actualEffects = [
  Effect("Reverb", "Room Size", "Wet", blueHue), 
  Effect("Delay", "Rate", "Wet", redHue), 
  Effect("Distortion", "Growl", "Wet", greenHue),
  Effect("Auto-Wah", "Rate", "Tone", brownHue)
]

totalFrameTestsPassed = 0
runs = 3
imagesPerRun = 11

### Execute the tests!! ###

for i in range(0, runs):

  print("Run " + str(i) + "-------------------------------------------------")

  expectedDataFile = "test-images/run" + str(i) + "/expected.csv"
  expected = getExpectedFromFile(expectedDataFile)

  runFrameTestsPassed = 0
  for j in range(0, imagesPerRun):
    imgPath = "test-images/run" + str(i) + "/frame" + str(j) + ".jpg"
    img = cv.imread(imgPath)

    logging.info("Frame " + str(j) + "\n")

    if img is None:
      raise ValueError("No image found in path " + imgPath)
    else:
      # Extract expected results for this capture
      convertToAudio(expected[j][0], expected[j][1], img, expectedEffects[0])
      convertToAudio(expected[j][2], expected[j][3], img, expectedEffects[1])
      convertToAudio(expected[j][4], expected[j][5], img, expectedEffects[2])
      convertToAudio(expected[j][6], expected[j][7], img, expectedEffects[3])

      # Do we want to reset the background?
      resetBackground = False
      if j == 0:
        firstFrame = img
        resetBackground = True
      
      # Extract actual results for this capture
      outImg, firstFrame, updateBackgroundSuccess = imageToEffects(img, firstFrame, resetBackground, actualEffects)

      # Output results for this run!
      testFailed = False
      for e in range(0, 4):
        if expectedEffects[e].effectName != actualEffects[e].effectName or not areClose(expectedEffects[e].x ,actualEffects[e].x, 0.09) or not areClose(expectedEffects[e].y, actualEffects[e].y, 0.09):
          testFailed = True
          logging.info("Expected Effect " + str(e) + " - " + str(expectedEffects[e].effectName) + " - " + str(expectedEffects[e].x) + ", " + str(expectedEffects[e].y))
          logging.info("Actual   Effect " + str(e) + " - " + str(actualEffects[e].effectName) + " - " + str(actualEffects[e].x) + ", " + str(actualEffects[e].y))
      
      if testFailed == True:
        print("Test Failed for Frame " + str(j) + " ❌")
      else:
        print("Test passed for Frame " + str(j) + " ✅")
        runFrameTestsPassed += 1

  totalFrameTestsPassed += runFrameTestsPassed 

print("\n" + str(totalFrameTestsPassed) + "/" + str(runs*11) + " frames successful")
