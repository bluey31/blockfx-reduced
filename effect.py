class Effect:
  """Represents an Effect and it's state

  Parameters
  ----------
  name : string
    The name of the effect
  xName : string
    The name of the first effect parameter
  yName : string
    The name of the second effect parameter
  associatedHue : int
    The hue value that this effect is associated with
  x : int
    Value for the first parameter
  y : int
    Value for the second parameter
  i : int
    Intensity of the overall effect
  used : bool
    Used as an indicator to set the effect on and off
"""
  def __init__(self, name, xName, yName, associatedHue, x=0, y=0, i=0):
    self.associatedHue = associatedHue
    self.used = 0
    self.effectName = name
    self.x = x
    self.xName = xName
    self.y = y
    self.yName = yName
    self.i = i

  def setDefault(self):
    self.x = 0
    self.y = 0
    self.i = 0
    self.used = 0