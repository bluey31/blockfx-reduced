import imutils
import logging
import cv2 as cv
import numpy as np
from sklearn.cluster import KMeans
from operator import itemgetter
from block import Block
from functools import reduce

"""Takes an image and updates a given list of effects based off the contents of the image using a LEGO brick extractor. 
   Requires a background frame to be used by the background subtraction step.

    
  Parameters
  ----------
  frame : Mat
    The frame we desire to extract Effect parameters from
  backgroundFrame : Mat
    The background frame that will be subtracted from the main frame
  reset : bool
    When enabled the reset flag, the background frame will be set to equal frame.
  effects : Effect []
    The list of effects we wish to popluate with parameters
"""
def imageToEffects(frame, backgroundFrame, reset, effects):

  resetSuccess = False
  capturedSuccessfully = False

  outImg = np.copy(frame)

  # Resize the frame, convert it to grayscale, and blur it to account for slight movement
  colouredBlurred = cv.GaussianBlur(frame, (31, 31), 0)
  gray = cv.cvtColor(colouredBlurred, cv.COLOR_BGR2GRAY)

  # Fetch positions of LEGO bricks from LEGO extractor
  blocks, resetSuccess, backgroundFrame = LEGOExtractor(gray, backgroundFrame, outImg, colouredBlurred, reset)

  # Reset effect parameters
  for e in effects:
    e.setDefault()
    e.used = 0

  # At this step we match a LEGO brick to an effect and use the blocks parameters to parameterize the effect
  for block in blocks:
    # Find an effect with a hue matching one of the detected contours
    for e in range(len(effects)):
      if areHuesClose(effects[e].associatedHue, block.hue) and not effects[e].used:
        # Update our effects based on the current contour  
        logging.info("Setting " + effects[e].effectName + " to (" + str(block.x) + "," + str(block.y) + ")")
        convertToAudio(block.x, block.y, outImg, effects[e])
        logging.info(effects[e].effectName + " now has parameters (" + str(effects[e].x) + "," + str(effects[e].y) + ")")
        break
      elif effects[e].associatedHue + 8 > 180 and block.hue < 8:
        convertToAudio(block.x, block.y, outImg, effects[e])
        break

  if reset == True:
    # Reset effects for this loop
    for e in effects:
      e.setDefault()
      e.used = 0

  return outImg, backgroundFrame, resetSuccess

"""The LEGO extractor model. Will detect and extract the LEGO brick position and sizes from the given image.

  Requires a background frame to be used by the background subtraction step.

  When the reset flag is enabled, the background frame used by the algorithm will be updated to frame.

  Parameters
  ----------
  frame : Mat
    The current frame containing the tangible features
  backgroundFrame : Mat
    The background frame to use for the background subtraction
  imgToDisplay : Mat
    The annotated image to display to the end user
  colouredBlurred : Mat
    The original coloured frame
  reset : bool
    When true, reset the background frame to be the given frame

  Returns
  -------
  validContours : [Rect]
    A list of contours that contain the position and size of the LEGO bricks in the image
  resetSuccess : bool
    A signal that is set to true once the function has successfully reset the background
  backgroundFrame : Mat
    The current background image, will be updated if the reset flag was set to true
"""
def LEGOExtractor(frame, backgroundFrame, imgToDisplay, colouredBlurred, reset):

  resetSuccess = False
  blocks = []

  # Allows the user to reset the background even if we aren't in capture mode
  if backgroundFrame is None or reset == True:
    logging.info("Successfully updated background")
    backgroundFrame = frame
    resetSuccess = True
  
  # Compute the absolute difference between the current frame and first frame
  frameDelta = cv.absdiff(backgroundFrame, frame)

  # Threshold to remove shadows
  thresh = cv.threshold(frameDelta, 40, 235, cv.THRESH_BINARY)[1]

  # Dilate image to fill holes
  kernel = np.ones((7,7), np.uint8)
  dilated = cv.dilate(thresh, kernel, iterations=1)

  # Fetch contours
  cnts = cv.findContours(dilated.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
  cnts = imutils.grab_contours(cnts)

  # Set the valid contour sizes
  minCnt = 4000
  maxCnt = 80000

  for c in cnts:
    if cv.contourArea(c) > minCnt and cv.contourArea(c) < maxCnt:
      # Compute the bounding box for the contour
      (x, y, w, h) = cv.boundingRect(c)
      # Crop colored image at contour
      colorImCrop = np.copy(colouredBlurred[int(y):int(y+h), int(x):int(x+w)])
      dominantHue = getDominantHue(colorImCrop)

      logging.info("Looking at brick with hue " + str(dominantHue))
      
      midX = x + int(w/2)
      midY = y + int(h/2)

      cv.rectangle(imgToDisplay, (x, y), (x + w, y + h), (255,255,255), 2)
      cv.circle(imgToDisplay, (midX, midY), 10, (255,255,255), 2)

      block = Block(midX, midY, dominantHue)
      blocks += [block]

  return blocks, resetSuccess, backgroundFrame

"""Updates a given effects parameters based on the x and y position of an object.

  Parameters
  ----------
  x : int
    The x position of the object
  y : int
    The y position of the object
  img : Mat
    The image the objects were extracted from
  effect : Effect
    The effect to update
"""
def convertToAudio(x, y, img, effect):
  maxY, maxX, _ = img.shape
  effectI = 0.6
  effectX = x / maxX
  effectY = 1 - (y / maxY) if y > 0 else 0.0 

  effect.used = 1
  effect.i = effectI
  effect.x = effectX
  effect.y = effectY

""" Returns true if h1 and h2 are the same plus or minus the provided tolerance value """
def areHuesClose(h1, h2, tolerance=10):
  return abs(h1 - h2) <= tolerance

""" Returns the dominant hue value across the provided contour using a Dictionary approach """
def getDominantHue(contour):
  hueAppearances = {}
  contour = cv.cvtColor(contour, cv.COLOR_BGR2HSV)
  pixelArray = np.array(contour)

  for pixelColumn in pixelArray:
    for pixel in pixelColumn:
      pixelHue = pixel[0]
      hueAppearances[pixelHue] = hueAppearances.get(pixelHue, 0) + 1  

  return 0 if len(hueAppearances.items()) == 0 else max(hueAppearances.items(), key=itemgetter(1))[0]
