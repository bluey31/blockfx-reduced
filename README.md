# BlockFX Reduced

Reduced code for the BlockFX thesis project. Originally the code was used to run on a Raspberry-Pi integrated device, seen below:

<img src="blockfx-pedal.png" width="200" height="300">

This repo contains the reduced codebase. I have only included the core image processing logic, and a test file to exercise it. Due to the highly integrated nature of the full application, this is the only way we can really see it building and working!

How it would work is that you'd place a brick in front of the device to apply an effect to the audio signal. Depending on the size, colour and location of the brick in the scene, the associative effects will be applied to the signal. 

*Note:* _I would not say this shows the full extent of what I consider my best work, just the best thing I had on hand for the interview!_ 

The layout of the reduced code is as follows...

### test.py

The test suite for `imagetoeffects.py`. Will do 3 runs using a set of 11 of images each time. The images for each run can be found in the `images/run` folders. Each `images/run` folder contains an `expected.csv`, which contains the expected results of applying `imagesToEffect` on each image. 

### imagetoeffects.py

The main image processing logic. The idea is that it will take in an image of lego bricks and map that into a set of Effects. Each effect will have it's parameters populated based on each lego bricks colour, position and size in the image.

For example, if we have a normal sized red LEGO brick representing reverb, placing it in the middle of the frame will produce an Effect object with x,y and intensity all set to 50%

### block.py and effect.py

Minimal OO-style classes to represent a Block and an Effect

## Getting started

To install the full list of dependencies, run

```
pip3 install imutils sklearn opencv-python numpy scipy scikit-image
```

Then, to run the test suite:
```
python3 test.py 
```

## TODO

In a hypothetical world where I am working on this properly, I would

- Productionise the code more, it is not super maintainable and I don't think it follows Python standards to their fullest extent
    - This includes refactoring some of the larger methods such as `imageToEffects` and `LEGOExtractor`
- Formalise the tests using a well-known unit testing framework, instead of rolling my own style
    - This will also enable a CI/CD pipeline to test before deployment 
- Make the tests exercise more of the internals and edge cases
    - Right now it's very integration-like in it's style, where we check the black box is giving us the right outputs given it's inputs
- In reality, I'd port this to C++ for performance reasons (Python was a good language for POC) 